const express = require('express');
const fs = require('fs');
const app = express();

const port = 3005;
const jsonDataFile = 'chatlines.json';


var messagesArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(messagesArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        messagesArray = JSON.parse(dataStr);
    }
}

loadAllData();

app.get('/send', (request, response) => {
    var name = request.query['name'];
    var color = request.query['color'];
    var msg = request.query['msg'];
    if (name == undefined || name == '' || color == '' || msg == '') {
        response.send("Error: invalid data sent");
        return;
    }
    
    var chatLine = {name: name, color: color, msg: msg};
    messagesArray.push(chatLine);
    saveAllData();
    response.send(`Message sent!`);
});

app.get('/update', (request, response) => {
    var html = "";
    for (var i = 0; i < messagesArray.length; i++) {
        var f = messagesArray[i];
        if (i == messagesArray.length - 1)
            html += `<option style="color:${f.color}" selected>${f.name}: ${f.msg}</option>`;
        else
            html += `<option style="color:${f.color}">${f.name}: ${f.msg}</option>`;
    }
    response.send(html);
});

app.get('/statistics', (request, response) => {
    var html = "";
    var msgs = {}, chats = {}; //

    for (var i = 0; i < messagesArray.length; i++) {
        if (msgs[messagesArray[i].name] == undefined) {
            msgs[messagesArray[i].name] = 1;
            chats[messagesArray[i].name] = messagesArray[i].msg.length;
        } else {
            msgs[messagesArray[i].name]++;
            chats[messagesArray[i].name] += messagesArray[i].msg.length;
        }
    }

    Object.keys(msgs).forEach(function (key) {
        console.log(key + ":" + msgs[key]);
        html += "<tr><td class='name'>"+key+"</td><td class='totalmsgs'>"+msgs[key]+"</td><td class='totalchars'>"+chats[key]+"</td></tr>";
    });
   
    response.send(html);
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});
