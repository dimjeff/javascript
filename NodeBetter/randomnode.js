const express = require('express');
const app = express();
const port = 3000;

app.get('/', (request, response) => {
  response.send('Hello from Express!');
});

var count = 0;
var historyArray = [];
app.get('/genrand', (request, response) => {
  var min = request.query.min;
  var max = request.query.max;
  if(min>max){
      response.send("error ");
      return;
  }

  var ran = Math.floor(Math.random() * parseInt(max-min) + 1)+parseInt(min);
  historyArray.push(ran);
  response.send(`${ran}`);
});

app.get('/history', (request, response) => {
    var strHtml="<ul>";
    for (var i = 0; i < historyArray.length; i++) {
        strHtml += "<li>"+historyArray[i]+"</li>";
    }
    strHtml+="</ul>";
  
  response.send(`${strHtml}`);
});

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
  
});


